# Organizing my Emacs init with org-mode #

The `emacs.org` file contains my actual configuration.
I still have an `init.el` file, but now I only use it to load `emacs.org`.
This one line is all you need to put in `init.el` to do that:

```lisp
(org-babel-load-file "~/.emacs.d/emacs.org")
```

[org-babel](https://orgmode.org/worg/org-contrib/babel/intro.html) allows you to embed blocks of code in an org file that can be executed.
The command `org-babel-load-file` takes an org file with embedded elisp code, extracts the elisp and writes it to an `.el` file, and then loads the `.el` file.
That process happens whenever `init.el` gets evaluated, either when Emacs starts up or when you run `eval-buffer` on it.

Since this slows down Emacs's startup time, some Emacs users prefer a different approach.
They write a file called `init.org` and run `org-babel-tangle` to generate their `init.el`, either manually (`C-c C-v t`) or with a hook to do it automatically on saving the file.
The advantage of this approach is that it makes Emacs start up faster.
I don't care about that because I run an [Emacs server](https://www.emacswiki.org/emacs/EmacsAsDaemon), so I don't usually notice how long it takes for Emacs to start up.

# Using use-package #

I use Emacs on several different machines and for a long time I just copied my configuration manually from one to the other.
This had several annoying side effects, but the most annoying was that I would have to futz with the config a couple times to get Emacs working.
I like to have `M-x` and `C-x C-f` rebound to counsel, and every time I started a new install of Emacs those commands would break because counsel wasn't installed, and I couldn't install counsel without `M-x`.
I'd have to go into `init.el` in Vim to comment out those keybindings long enough to get those packages installed.
(I could have used `emacs -q` but I enjoyed the irony.)
This got annoying enough that I finally sat down and learned how to use `use-package`.
It's really convenient because now when I put Emacs on a new system and start it up with my config, it just goes and downloads the packages I need automatically and they work right away.
